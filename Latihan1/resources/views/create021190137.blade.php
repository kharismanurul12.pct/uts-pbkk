    <form action="{{ ('/proses') }}" method="POST">

    <fieldset>
        <legend>Biodata Diri</legend>
        <table border="0" width="50%">
            <tr>
                <td>NPM</td>
                <td><input type="text" name="npm"></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td><input type="text" name="nama"></td>
            </tr>
            <tr>
                <td>Program Studi</td>
                <td><input type="text" name="prodi"></td>
            </tr>
            <tr>
                <td>NO. HP</td>
                <td><input type="tel" pattern="\(\d\d\d\d\)-\d\d\d\d\d\d\d\d" name="nohp" placeholder="(9999)-999999999"></td>
            </tr>
            <tr>
                <td>Tempat Tanggal Lahir</td>
                <td><input type="text" name="ttl" placeholder="Kota" >
                    <input type="date" name="ttl">
                </td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>
                    <label>
                        <input type="radio" name="kelamin">Laki-laki
                    </label>
                    <label>
                        <input type="radio" name="kelamin">Perempuan
                    </label>
                </td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>
                <select name="agama">
                    <option value="Islam">Islam</option>
                    <option value="Kristen">Kristen</option>
                    <option value="Katolik">Katolik</option>
                    <option value="Hindu">Hindu</option>
                    <option value="Budha">Budha</option>
                    <option value="Konghucu">Konghucu</option>
                </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" name="submit"></td>
            </tr>
    </fieldset>
    </form>
