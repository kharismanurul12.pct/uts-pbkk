<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{
    public function index(){
        return view('create');
    }

    public function proses(Request $request){
        $data = array();
        $data['npm'] = $request->npm;
        $data['nama'] = $request->nama;
        $data['prodi'] = $request->prodi;
        $data['nohp'] = $request->nohp;
        $data['ttl'] = $request->ttl;
        $data['kelamin'] = $request->kelamin;
        $data['agama'] = $request->agama;

        return view('view', ['data' => $data]);
    }
}