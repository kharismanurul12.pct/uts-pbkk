<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProsesController, 'index']);
Route::post('/proses', [ProsesController, 'proses']);

Route::post('/npm', [ProsesController, 'npm']);
Route::post('/nama', [ProsesController, 'nama']);
Route::post('/prodi', [ProsesController, 'prodi']);
Route::post('/nohp', [ProsesController, 'nohp']);
Route::post('/ttl', [ProsesController, 'ttl']);
Route::post('/kelamin', [ProsesController, 'kelamin']);
Route::post('/agama', [ProsesController, 'agama']);